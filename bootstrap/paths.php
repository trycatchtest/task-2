<?php
return [
    'base' => $baseDir,

    'bootstrap' => $baseDir . DIRECTORY_SEPARATOR . 'bootstrap',

    'config' => $baseDir . DIRECTORY_SEPARATOR . 'config',

    'public' => $baseDir . DIRECTORY_SEPARATOR . 'public',

    'resources' => $baseDir . DIRECTORY_SEPARATOR . 'resources',
];