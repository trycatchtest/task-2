<?php

function setupAppAutoloader()
{
    $baseDir = dirname(dirname(__FILE__));
    $srcDir = $baseDir . DIRECTORY_SEPARATOR . 'src';
    $appDir = $srcDir . DIRECTORY_SEPARATOR . 'App';

    require_once $appDir . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Core' . DIRECTORY_SEPARATOR . 'ClassLoader.php';

    App\Lib\Core\ClassLoader::setBaseDirectory($srcDir);

    spl_autoload_register(['App\Lib\Core\ClassLoader', 'autoload']);
}

setupAppAutoloader();

App\Lib\Core\ClassLoader::registerNamespace('App');

