<?php
if(!function_exists('pr')){

    function pr($data)
    {
        if(php_sapi_name() == 'cli'){
            print_r($data);
        }
        else{
            echo '<pre>' . print_r($data, true) . '</pre>';
        }
    }
}
