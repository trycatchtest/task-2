# Example architecture for REST application

## Directory structure

* `bootstrap` - setup autoloader and tools
* `config` - application and modules config files
* `public` - application webroot
* `resources` - application data files
* `src/App` - application code
* `tests` - unit tests
* `vendor` - external dependencies

### src/App

Application will be composed from modules containing business logic and helper classes (in `Lib` folder). 
Helper classes will provide base building blocks and different type of interfaces to the application. 
Currently there is only Http (REST) interface, one possible step will be to implement Console interface or Html interface. 

#### Lib

Application non business logic

* `Common` - base classes used in application, for example
    * `Entity` - base model for application data
    * `Collection`, `EntityCollection` - classes for managing collections of objects
    * `Service` - base class for building services
* `Core` - core building blocks
    * `Application` - application foundation and simple DI container
    * `BaseServiceProvider` - starting point for building module service providers 
    * `ClassLoader` - PSR-4 style autoloader 
    * `Config` - Config repository
    * `Exception` - Common parent for application exceptions
* `Datasources` - classes responsible for exchanging data with external data sources like files, db's, web services etc.
    * `Csv` - csv files manipulation, needs refactoring - add common data source interface, extract and abstract file manipulation methods
* `Http`
    * `HttpClient` - web application, responsible for managing web request to the application  
    * `Routing` - manage web endpoints, application respond for RESTful style routing, request parameters are taken from query string and request body
        * `Route` - web endpoint representation  
        * `RouteCollection` - application routes collection
        * `Router` - finds route for current request, builds URLs from named routes
    * `Request` - web request abstraction, populate request data 
    * `Dispatcher` - responsible for running appropriate controller action in response to web request
    * `Controller` - parent (not required) for controller classes
    * `Response` - handle sending response to client
        * `Response` - base response, responsible for sending response headers and body, needs further refactoring/extensions, especially with response headers
        * `RedirectResponse` - helper class for making redirect responses
        * `ErrorResponse` - helper class for rendering error responses
    
#### AddressBook

Application example module
 
* `Address` - business logic for managing address entities
    * `Address` - entity representing single address 
    * `AddressService` - it manages process of creating, updating and deleting entities
    * `AddressValidator` - address validation rules
    * `Repositories` - different kinds of repositories which provides data to the application  
        * `RepositoryInterface` - all repositories must implement it to maintain compatibility with application logic
        * `BaseRepository` - helper class for building repositories, it provides basic building methods for entity and collection of entities
        * `CsvRepository` - it acts as a data mapper between data source and application logic
* `Http/Controllers` - module Http interface
    * `AddressesController` - RESTful CRUD implementation  
* `ServiceProvider` - module setup class, responsible for creating and initialising module classes according to configuration (eg. loading routes, creating services)

#### Routing

While defining routes one must provide a base route url, like "/address" (url is attached to front controller index.php eg. index.php/address),
a request method (GET, POST, PUT, DELETE) and optionally array of parameters which are required to be present in query string to consider,
route definition must have also an action address (composed from controller class name and action name) and optionally name for easier url generation

eg. in `ServiceProvider`  
`$this->app->router()->routes()->post('/address', ['action' => 'ControllerClass::controller_action', 'inQuery' => ['id'], 'name' => 'address.delete']);`  
this route will react for POST request to `/index.php/address?id=ID`, where ID is some kind of key,  
in response for such request dispatcher will run `controller_action` in `ControllerClass`

`$this->app->router()->make('address.delete', ['id' => 'ID'])`  
will generate url to above endpoint

Router can be extended to handle "pretty" urls by introducing some kind of regular expression processing in route definitions 

#### DI container

Application introduces basic dependency injection container. All classes which are resolved through application
and implementing `InjectableInterface` can define external dependencies. Dependencies are injected into objects if
there are present in container and object has proper setter methods

eg. in `AdressService`  

```php
protected $injectable = [
    'repository' => 'App\AddressBook\Address\Repositories\RepositoryInterface',
    'validator' => 'App\AddressBook\Address\AddressValidator',
];

public function setRepository(RepositoryInterface $repository)
{
    $this->repository = $repository;
}

public function setValidator(AddressValidator $validator)
{
    $this->validator = $validator;
}
```

### Bootstrap

Resources needed to bootstrap application, setup autoloader, define project paths, include some tools

### Config

Config files for application and modules, each config is a file which returns php array of config values,
values are available in application as prefix.key pairs, where prefix is config file name without extension
 
### Public

Application webroot, index.php is starting point for web client

### Resources

Data files used by application, like csv files, sql lite db's etc.

### Tests

Application tests, divided by test type, units, integration etc.

### Vendor

External dependencies like testing or other kinds of libraries 

## TODO

* more unit tests, especially for application logic, 
* some integration tests
* more built-in validator rules
* other data sources
