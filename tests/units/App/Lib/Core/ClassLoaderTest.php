<?php namespace App\Lib\Core;

class ClassLoaderTest extends \PHPUnit_Framework_TestCase
{
    public function testAutoloadingWorks()
    {
        $this->assertEquals('App\Lib\Core\ClassLoader', ClassLoader::class);
    }
}