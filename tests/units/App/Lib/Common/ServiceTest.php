<?php namespace App\Lib\Common;

class TestService extends Service
{

}

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Service
     */
    protected $service;

    public function testImplementsInjectableInterface()
    {
        $this->assertInstanceOf('App\Lib\Common\InjectableInterface', $this->service);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->service = new TestService();
    }

    
}
