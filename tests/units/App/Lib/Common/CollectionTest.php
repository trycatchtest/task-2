<?php namespace App\Lib\Common;

class CollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Collection
     */
    protected $collection;

    protected function setUp()
    {
        parent::setUp();

        $this->collection = new Collection();
    }

    public function testCreating()
    {
        $data = ['foo', 'bar'];
        $collection = new Collection($data);

        $result = $collection->all();

        $this->assertEquals($data, $result);
    }

    public function testHas()
    {
        $key = 'foo';
        $value = 'bar';

        $this->assertFalse($this->collection->has($key));

        $this->collection->put($key, $value);

        $this->assertTrue($this->collection->has($key));
    }

    public function testGet()
    {
        $key = 'foo';
        $value = 'bar';

        $this->collection->put($key, $value);

        $this->assertEquals($value, $this->collection->get($key));
    }

    public function testGetDefault()
    {
        $key = 'foo';
        $default = 'baz';

        $this->assertNull($this->collection->get($key));

        $this->assertEquals($default, $this->collection->get($key, $default));
    }

    public function testPut()
    {
        $key = 'foo';
        $value = 'bar';

        $this->collection->put($key, $value);

        $this->assertEquals($value, $this->collection->get($key));

    }

    public function testPush()
    {
        $value = 'bar';
        $value2 = 'baz';

        $this->collection->push($value);
        $this->collection->push($value2);

        $this->assertEquals($value2, array_pop($this->collection->all()));
    }

    public function testPull()
    {
        $key = 'foo';
        $value = 'bar';
        $this->collection->put($key, $value);

        $result = $this->collection->pull($key);

        $this->assertEquals($value, $result);
        $this->assertNull($this->collection->get($key));
    }

    public function testPullDefault()
    {
        $key = 'foo';
        $default = 'bar';

        $result = $this->collection->pull($key, $default);

        $this->assertEquals($default, $result);
        $this->assertNull($this->collection->get($key));
    }

    public function testForget()
    {
        $key = 'foo';
        $value = 'bar';
        $this->collection->put($key, $value);

        $this->collection->forget($key);

        $this->assertNull($this->collection->get($key));
    }

    public function testGetAll()
    {
        $key = 'foo';
        $value = 'bar';
        $this->collection->put($key, $value);

        $result = $this->collection->all();

        $this->assertEquals([$key => $value], $result);
    }

    public function testGetIterator()
    {
        $key = 'foo';
        $value = 'bar';
        $this->collection->put($key, $value);

        $this->assertInstanceOf('\ArrayIterator', $this->collection->getIterator());

        $this->assertEquals(1, $this->collection->getIterator()->count());
    }

    public function testCount()
    {
        $key = 'foo';
        $value = 'bar';
        $this->collection->put($key, $value);

        $this->assertEquals(1, count($this->collection->all()));
    }

}
