<?php namespace App\Lib\Common;

class TestEntity extends Entity
{
    protected $foo;
    protected $bar;
    protected $baz;
    protected $fillable = ['foo', 'bar', 'baz'];
}

class EntityTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Entity
     */
    protected $entity;

    public function testCreatingDefault()
    {
        $entity = new TestEntity();

        $this->assertNull($entity->getKey());
        $this->assertTrue($entity->exists());
        $this->assertFalse($entity->dirty());
    }

    public function testCreatingNew()
    {
        $entity = new TestEntity(null, false, true);

        $this->assertNull($entity->getKey());
        $this->assertFalse($entity->exists());
        $this->assertTrue($entity->dirty());
    }

    public function testCreatingWithKey()
    {
        $key = 'foo';

        $entity = new TestEntity($key);

        $this->assertEquals($key, $entity->getKey());
        $this->assertTrue($entity->exists());
        $this->assertFalse($entity->dirty());
    }

    public function testSetDirty()
    {
        $this->entity->setDirty(true);
        $this->assertTrue($this->entity->dirty());

        $this->entity->setDirty(false);
        $this->assertFalse($this->entity->dirty());

        $this->entity->setDirty(true);
        $this->assertTrue($this->entity->dirty());
    }

    public function testStoredIsSettingProperties()
    {
        $key  = 'foo';
        $entity = new TestEntity(null, false, true);

        $entity->stored($key);

        $this->assertEquals($key, $entity->getKey());
        $this->assertTrue($entity->exists());
        $this->assertFalse($entity->dirty());
    }

    public function testGetKey()
    {
        $key = 'foo';

        $entity = new TestEntity($key);

        $this->assertEquals($key, $entity->getKey());

    }

    public function testSetAttributes()
    {
        $data = [
            'foo' => 'foo-val',
            'bar' => 'bar-val',
            'baz' => 'baz-val',
        ];

        $this->entity->setAttributes($data);

        $this->assertEquals($data, $this->entity->getAttributes());
    }

    public function testSetSomeAttributes()
    {
        $data = [
            'foo' => 'foo-val',
            'baz' => 'baz-val',
        ];
        $expected = [
            'foo' => 'foo-val',
            'bar' => '',
            'baz' => 'baz-val',
        ];

        $this->entity->setAttributes($data);

        $this->assertEquals($expected, $this->entity->getAttributes());
    }

    public function testSetAttributesAreNotOverwritten()
    {
        $data = [
            'foo' => 'foo-val',
            'baz' => 'baz-val',
        ];
        $data2 = [
            'bar' => 'bar-val',
        ];

        $expected = [
            'foo' => 'foo-val',
            'bar' => 'bar-val',
            'baz' => 'baz-val',
        ];

        $this->entity->setAttributes($data);
        $this->entity->setAttributes($data2);

        $this->assertEquals($expected, $this->entity->getAttributes());
    }

    public function testImplementsArrayableInterface()
    {
        $this->assertInstanceOf('App\Lib\Common\ArrayableInterface', $this->entity);
    }

    public function testToArray()
    {
        $data = [
            'foo' => 'foo-val',
            'bar' => 'bar-val',
            'baz' => 'baz-val',
        ];

        $this->entity->setAttributes($data);

        $this->assertEquals($data, $this->entity->toArray());

    }


    protected function setUp()
    {
        parent::setUp();

        $this->entity = new TestEntity();
    }

    
}
