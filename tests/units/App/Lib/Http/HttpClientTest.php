<?php namespace App\Lib\Http;

class HttpClientTest extends \PHPUnit_Framework_TestCase
{
    public function testHttpClientIsApplication()
    {
        $client = new HttpClient();

        $this->assertInstanceOf('App\Lib\Core\Application', $client);
    }
}
