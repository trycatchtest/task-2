<?php
return [
    'debug' => 1,

    'providers' => [
        '\App\AddressBook\ServiceProvider',
    ]
];