<?php

return [
    'resourcePath' => 'resources',

    'repositoryConfig' => [
        'App\AddressBook\Address\Repositories\CsvRepository' => [
            'table' => 'addresses.csv',
            'map' => [
                'name',
                'phone',
                'street',
            ]
        ]
    ],

    'csv-repositories' => [
        'App\AddressBook\Address\Repositories\RepositoryInterface' => 'App\AddressBook\Address\Repositories\CsvRepository'
    ],

    'services' => [
        'App\AddressBook\Address\AddressService'
    ]
];