<?php namespace App\Lib\Validation;

use App\Lib\Common\Collection;

class Validator
{
    /**
     * @var Collection
     */
    protected $data;
    /**
     * @var array
     */
    protected $errors;

    public function validate(array $data = [], array $rules = [])
    {
        $this->data = new Collection($data);
        
        $this->errors = [];

        foreach ($rules as $attribute => $attributeRules) {
            $value = $this->data->get($attribute);

            foreach($attributeRules as $ruleDefinition){
                list($rule, $options) = $this->parseDefinition($ruleDefinition);

                try{
                    if(false === $this->doValidation($rule, $value, $attribute, $options)){
                        $this->addError($attribute, $rule);
                    }
                }
                catch(DataHasNoAttributeException $e){
                    break;
                }
            }
        }

        if(count($this->errors)){
            throw new ValidationException(sprintf("Validation failed: \n%s", implode("\n",$this->errors)));
        }
    }

    protected function parseDefinition($ruleDefinition)
    {
        $options = null;
        if(strpos($ruleDefinition, ':')){ //: on position grater than 0
            list($rule, $options) = explode(':', $ruleDefinition, 2);
        }
        else{
            $rule = $ruleDefinition;
        }

        return [$rule, $options];
    }

    protected function shouldValidate($attribute)
    {

    }

    protected function doValidation($rule, $value, $attribute, $options)
    {
        $method = $this->findMethod($rule);

        return $this->$method($value, $attribute, $options);
    }

    protected function dataHasAttribute($attribute){
        return $this->data->has($attribute);
    }

    protected function addError($attribute, $rule)
    {
        $this->errors[] = sprintf('%s:%s', $attribute, $rule);
    }


    public function validateRequired($value)
    {
        if(is_null($value)){
            return false;
        }
        if(is_string($value) && trim($value) === ""){
            return false;
        }
        if(is_array($value) && count($value) == 0){
            return false;
        }
    }

    public function validateIfDataExists($value, $attribute)
    {
        if(!$this->dataHasAttribute($attribute)){
            throw new DataHasNoAttributeException;
        }
    }

    public function validateNumeric($value)
    {
        return is_numeric($value);
    }

    public function validateLength($value, $attribute, $length)
    {
        $length = (int) $length;

        if(is_string($value)){
            return mb_strlen($value) == $length;
        }

        if(is_array($value)){
            return count($value) == $length;
        }

        return false;
    }


    protected function findMethod($ruleName)
    {
        $methodName = 'validate' .
                        str_replace(' ', '',
                            ucwords(
                                str_replace('-', ' ', $ruleName)
                            )
                        );

        if(method_exists($this, $methodName)){
            return $methodName;
        }
    }
}