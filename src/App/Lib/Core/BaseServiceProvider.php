<?php namespace App\Lib\Core;

use App\Lib\Http\HttpClient;

abstract class BaseServiceProvider implements ServiceProviderInterface
{
    /**
     * @var ApplicationInterface
     */
    protected $app;

    /**
     * @param ApplicationInterface $app
     */
    public function __construct(ApplicationInterface $app)
    {
        $this->app = $app;
    }

    public function httpClient()
    {
        return $this->app instanceof HttpClient;
    }

    abstract public function bootstrap();

}