<?php namespace App\Lib\Core;

use App\Lib\Common\Collection;
use App\Lib\Common\InjectableInterface;

abstract class Application implements ApplicationInterface
{
    /**
     * @var Collection
     */
    protected $paths;

    /**
     * @var Collection
     */
    protected $container;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Collection
     */
    protected $providers;

    /**
     * @param array $paths
     */
    public function __construct(array $paths = [])
    {
        $this->container = new Collection();

        $this->providers = new Collection();

        $this->paths = new Collection($paths);

        $this->config = new Config($this->paths->get('config'));

        $this->bootstrap();
    }

    protected function bootstrap()
    {
        $this->setupConfig();

        $this->setupEnv();

        $this->bootstrapServiceProviders();
    }

    protected function setupConfig()
    {
        $this->config->load('app');
    }

    protected function setupEnv()
    {
        if($this->config->get('app.debug')){
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
        else{
            error_reporting(E_ALL & ~E_NOTICE);
            ini_set('display_errors', 0);
        }
    }

    protected function bootstrapServiceProviders()
    {
        $providers = $this->config->get('app.providers', []);

        foreach($providers as $providerClass){
            if(!$this->providers->has($providerClass)){
                $provider = new $providerClass($this);

                $provider->bootstrap();

                $this->providers->put($providerClass, $provider);
            }
        }
    }


    /**
     * @param string $abstract
     * @param object $concrete
     */
    public function bind($abstract, $concrete)
    {
        return $this->container->put($abstract, $concrete);
    }

    /**
     * @param string $abstract
     * @param mixed $default
     * @return object
     */
    public function instance($abstract, $default = null)
    {
        if(!$this->container->has($abstract)){
            return $default;
        }

        $concrete = $this->container->get($abstract);

        $this->injectDependencies($concrete);

        return $concrete;
    }

    public function make($product, array $constructorParameters = [])
    {
        if($this->container->has($product)){
            return $this->instance($product);
        }

        $reflection = new \ReflectionClass($product);

        $constructor = $reflection->getConstructor();

        if(is_null($constructor)){
            $concrete = new $product;
        }
        else{
            $concrete = $reflection->newInstanceArgs($constructorParameters);
        }

        $this->injectDependencies($concrete);

        return $concrete;
    }

    /**
     * @param InjectableInterface $receiver
     */
    public function injectDependencies($receiver)
    {
        if(! ($receiver instanceof InjectableInterface)){
            return;
        }

        $injectable = $receiver->getInjectable();

        foreach($injectable as $attribute => $abstract){

            $instance = $this->make($abstract);

            $method = 'set' . ucfirst($attribute);

            $receiver->$method($instance);
        }
    }

    /**
     * @return Config
     */
    public function config()
    {
        return $this->config;
    }

    /**
     * @return Collection
     */
    public function paths()
    {
        return $this->paths;
    }


}