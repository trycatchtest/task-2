<?php namespace App\Lib\Core;

interface ServiceProviderInterface 
{
    public function bootstrap();
}