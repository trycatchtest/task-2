<?php namespace App\Lib\Core;

use \Exception as BaseException;

class Exception extends BaseException
{
    protected $status = 500;

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


}