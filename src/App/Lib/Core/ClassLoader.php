<?php namespace App\Lib\Core;

class ClassLoader 
{
    protected static $namespaces = [];

    protected static $baseDirectory;

    public static function setBaseDirectory($baseDirectory)
    {
        self::$baseDirectory = $baseDirectory;
    }

    public static function registerNamespace($namespacePrefix, $baseDirectory = null)
    {
        self::$namespaces[$namespacePrefix .'\\'] = ($baseDirectory ?: self::$baseDirectory). DIRECTORY_SEPARATOR . $namespacePrefix;
    }

    public static function autoload($className)
    {
        foreach(self::$namespaces as $namespacePrefix => $baseDir) {

            if(strpos($className, $namespacePrefix) !== 0){
                continue;
            }

            $relativeClassName = substr($className, strlen($namespacePrefix));

            $classFile = $baseDir . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $relativeClassName) . '.php';

            if(is_readable($classFile)){
                include $classFile;

                return;
            }
        }
    }
}