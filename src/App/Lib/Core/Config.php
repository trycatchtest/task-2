<?php namespace App\Lib\Core;

use App\Lib\Common\Collection;

class Config extends Collection
{
    protected $paths;

    public function __construct($paths = [])
    {
        parent::__construct();

        if(!is_array($paths)){
            $paths = (array) $paths;
        }

        $this->paths = $paths;
    }

    public function load($name)
    {
        $fileName = $name . '.php';
        foreach($this->paths as $path){
            $configFile = $path . DIRECTORY_SEPARATOR . $fileName;

            if(is_readable($configFile)){
                $config = include $configFile;

                $this->put($name, new Collection((array) $config));

                break;
            }
        }

    }

    public function get($key, $default = null)
    {
        if(strpos($key, '.')){
            list($group, $key) = explode('.', $key, 2);

            if($this->has($group)){
                return $this->get($group)->get($key, $default);
            }
            else{
                return $default;
            }
        }

        return parent::get($key, $default);
    }


}