<?php namespace App\Lib\Core;

use App\Lib\Common\InjectableInterface;

interface ApplicationInterface
{
    public function run();

    /**
     * @param string $abstract
     * @param object $concrete
     */
    public function bind($abstract, $concrete);

    /**
     * @param string $abstract
     * @param mixed $default
     * @return object
     */
    public function instance($abstract, $default = null);

    /**
     * @return Config
     */
    public function config();

    /**
     * @return Collection
     */
    public function paths();

    /**
     * @param InjectableInterface $receiver
     */
    public function injectDependencies($receiver);
}