<?php namespace App\Lib\Common;

abstract class Entity implements ArrayableInterface
{
    /**
     * @var mixed
     */
    protected $key;
    /**
     * @var bool
     */
    protected $exists;
    /**
     * @var bool
     */
    protected $dirty;
    /**
     * @var array
     */
    protected $fillable;

    /**
     * @param mixed $key
     * @param bool $exists
     * @param bool $dirty
     */
    public function __construct($key = null, $exists = true, $dirty = false)
    {
        $this->key = $key;
        $this->exists = $exists;
        $this->dirty = $dirty;
    }

    /**
     * @param bool $state
     */
    public function setDirty($state = true)
    {
        $this->dirty = $state;
    }

    /**
     * @return bool
     */
    public function dirty()
    {
        return $this->dirty;
    }

    /**
     * @param $key
     */
    public function stored($key)
    {
        $this->exists = true;

        $this->dirty = false;

        $this->key = $key;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->getAttributes();
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return boolean
     */
    public function exists()
    {
        return $this->exists;
    }

    /**
     * @param array $data
     */
    public function setAttributes(array $data = [])
    {
        $data = new Collection($data);

        foreach ($this->fillable as $attribute) {
            if(!$data->has($attribute)) {
                continue;
            }

            $newValue = $data->get($attribute);

            if($this->$attribute === $newValue){
                continue;
            }

            if(!$this->dirty){
                $this->dirty = true;
            }

            $this->$attribute = $newValue;
        }
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        $result = [];

        foreach ($this->fillable as $attribute) {
            $result[$attribute] = $this->$attribute;
        }

        return $result;
    }
}