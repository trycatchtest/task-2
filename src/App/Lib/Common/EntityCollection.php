<?php namespace App\Lib\Common;

class EntityCollection extends Collection implements ArrayableInterface
{
    /**
     * @return array
     */
    public function toArray()
    {
        $result = [];

        foreach($this->items as $entity){
            $result[] = $entity->getAttributes();
        }

        return $result;
    }

}
