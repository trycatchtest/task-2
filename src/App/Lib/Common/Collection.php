<?php namespace App\Lib\Common;

class Collection
{
    protected $items;

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->items);
    }

    public function get($key, $default = null)
    {
        if($this->has($key)){
            return $this->items[$key];
        }

        return $default;
    }

    public function put($key, $value)
    {
        $this->items[$key] = $value;
    }

    public function push($value)
    {
        $this->items[] = $value;
    }

    public function pull($key, $default = null)
    {
        $value = $this->get($key, $default);

        $this->forget($key);

        return $value;
    }

    public function forget($key)
    {
        if($this->has($key)){
            unset($this->items[$key]);
        }
    }

    public function all()
    {
        return $this->items;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }
}