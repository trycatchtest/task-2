<?php namespace App\Lib\Common;

interface ArrayableInterface 
{
    /**
     * @return array
     */
    public function toArray();
}