<?php namespace App\Lib\Common;

interface InjectableInterface
{
    public function getInjectable();
}