<?php namespace App\Lib\Common;

abstract class Service implements InjectableInterface
{
    protected $repository;

    /**
     * @var array
     */
    protected $injectable = [];

    public function getInjectable()
    {
        return $this->injectable;
    }

    public function getRepository()
    {
        return $this->repository;
    }
}