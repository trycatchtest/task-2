<?php namespace App\Lib\Datasources;

use App\Lib\Common\Collection;
use App\Lib\Datasources\Exceptions\IncompleteDataException;
use App\Lib\Datasources\Exceptions\ResourceNotFoundException;
use App\Lib\Datasources\Exceptions\SourceNotAccessibleException;

class Csv
{
    /**
     * @var string
     */
    protected $resourcePath;

    /**
     * @var array
     */
    protected $fieldMap;

    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var resource
     */
    protected $file;

    public function __construct($resourcePath, array $fieldMap)
    {
        $this->resourcePath = $resourcePath;

        $this->fieldMap = $fieldMap;

        $this->data = new Collection();
    }


    public function create($fields)
    {
        $this->ensureFieldsPresent($fields);

        $this->load();

        return $this->append($fields);
    }

    public function read($key = null)
    {
        $this->load();

        if(is_null($key)){
            return $this->data->all();
        }

        $key = $this->normalizeKey($key);

        $this->ensureExists($key);

        return $this->data->get($key);
    }

    public function update($key, $fields)
    {
        $this->ensureFieldsPresent($fields);

        $this->load();

        $key = $this->normalizeKey($key);

        $this->ensureExists($key);

        $this->data->put($key, $fields);

        $this->dump();
    }

    public function delete($key)
    {
        $this->load();

        $key = $this->normalizeKey($key);

        $this->ensureExists($key);

        $this->data->forget($key);

        $this->dump();
    }


    protected function load()
    {
        $this->open();

        while(($line = fgetcsv($this->file)) !== false){
            $key = array_shift($line);

            if(is_numeric($key)){
                $this->data->put($key, array_combine($this->fieldMap, $line));
            }
        }

        $this->close();
    }

    protected function dump()
    {
        $this->open('w');

        $rows = $this->data->all();

        foreach($rows as $key => $row){
            $fields = $this->mapFields($row);

            array_unshift($fields, $key);

            fputcsv($this->file, $fields);
        }

        $this->close();
    }

    protected function append($fields)
    {
        $this->open('a');

        $fields = $this->mapFields($fields);

        $newId = $this->findNextId();

        array_unshift($fields, $newId);

        fputcsv($this->file, $fields);

        $this->close();

        return $newId;
    }

    protected function findNextId()
    {
        $rows = $this->data->all();

        $maxId = 0;
        foreach($rows as $key => $row){
            if($key > $maxId){
                $maxId = $key;
            }
        }

        return ($maxId + 1);
    }

    protected function mapFields($fields)
    {
        $row = [];

        foreach($this->fieldMap as $field){
            $row[] = $fields[$field];
        }

        return $row;
    }

    protected function normalizeKey($key)
    {
        if(!is_numeric($key)){
            throw new ResourceNotFoundException(sprintf("%s: %s", $this->resourcePath, $key));
        }

        return (int) $key;
    }


    protected function ensureFieldsPresent($fields)
    {
        $fieldNames = array_keys($fields);

        $missing = array_diff($this->fieldMap, $fieldNames);

        if(count($missing)){
            throw new IncompleteDataException(sprintf('Incomplete data, missing fields: %s', implode(', ', $missing)));
        }
    }

    protected function ensureExists($key)
    {
        if(!$this->data->has($key)){
            throw new ResourceNotFoundException(sprintf("%s: %s", $this->resourcePath, $key));
        }

        return true;
    }

    protected function ensureNotDeleted($key)
    {
        $data = $this->data->get($key);

        $notEmpty = array_filter($data, function($value){
            return 'NULL' != $value;
        });

        if(count($notEmpty)){
            return;
        }

        throw new ResourceNotFoundException(sprintf("%s: %s", $this->resourcePath, $key));
    }

    protected function ensureAccessible()
    {
        if(!is_writable($this->resourcePath)){
            throw new SourceNotAccessibleException($this->resourcePath);
        }
    }


    protected function open($mode = 'r')
    {
        $this->ensureAccessible();

        $this->file = fopen($this->resourcePath, $mode);

        if(!$this->file){
            throw new SourceNotAccessibleException($this->resourcePath);
        }

        if($mode == 'r'){
            $this->acquireReadLock();
        }
        else{
            $this->acquireWriteLock();
        }
    }

    protected function acquireReadLock()
    {
        flock($this->file, LOCK_SH);
    }

    protected function acquireWriteLock()
    {
        flock($this->file, LOCK_EX);
    }

    protected function close()
    {
        $this->releaseLock();

        fclose($this->file);
    }

    protected function releaseLock()
    {
        flock($this->file, LOCK_UN);
    }
}