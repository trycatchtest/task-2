<?php namespace App\Lib\Datasources\Exceptions;

class ResourceNotFoundException extends ResourceException
{
    protected $status = 404;
}