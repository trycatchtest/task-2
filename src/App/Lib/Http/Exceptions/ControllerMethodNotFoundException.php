<?php namespace App\Lib\Http\Exceptions;

use App\Lib\Core\Exception;

class ControllerMethodNotFoundException extends Exception
{

}