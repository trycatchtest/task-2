<?php namespace App\Lib\Http;

use App\Lib\Common\Collection;

trait ControllerTrait
{
    public function setQuery(Collection $query)
    {
        $this->query = $query;
    }

    public function query()
    {
        return $this->query;
    }

    public function setData(Collection $data)
    {
        $this->data = $data;
    }

    public function data()
    {
        return $this->data;
    }
}