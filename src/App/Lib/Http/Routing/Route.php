<?php namespace App\Lib\Http\Routing;

use App\Lib\Common\Collection;

class Route
{
    /**
     * @var string
     */
    protected $url;
    /**
     * @var Collection
     */
    protected $options;

    /**
     * @param $url
     * @param array $options
     */
    public function __construct($url, array $options = [])
    {
        $this->url = $url;

        $this->options = new Collection($options);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function name()
    {
        return $this->options->get('name');
    }

    public function method()
    {
        return $this->options->get('method');
    }

    public function action()
    {
        return $this->options->get('action');
    }
}