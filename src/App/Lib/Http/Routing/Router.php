<?php namespace App\Lib\Http\Routing;

use App\Lib\Http\Request;
use App\Lib\Http\Routing\Exceptions\NotFoundException;
use App\Lib\Http\Routing\Exceptions\RequiredParametersMissingException;

class Router
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var RouteCollection
     */
    protected $routes;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var Route
     */
    protected $current;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->routes = new RouteCollection();

        $this->path = $request->path();
    }

    /**
     * @return RouteCollection
     */
    public function routes()
    {
        return $this->routes;
    }

    /**
     * @param string $path
     * @throws NotFoundException
     */
    public function match($path = null)
    {
        $path = $path ?: $this->path;

        $routes = $this->routes->getIterator();

        $method = $this->request->method();

        foreach($routes as $route => $options){
            if($this->isMatching($route, $path, $options, $method)){
                return;
            }
        }

        throw new NotFoundException($path);
    }

    protected function isMatching($route, $path, $routeOptions, $method = 'GET')
    {
        $inQueryParams = array_keys($this->request->query()->all());

        if(!preg_match("\1^$route$\1", $path)) {
            return;
        }

        foreach($routeOptions as $routeOption){
            if($method != $routeOption['method']){
                continue;
            }

            if(!array_key_exists('inQuery', $routeOption)) {
                $this->makeCurrent($route, $routeOption);
                return true;
            }

            if($this->allParamsPresent($inQueryParams, $routeOption['inQuery'])){
                $this->makeCurrent($route, $routeOption);
                return true;
            }
        }
    }

    protected function makeCurrent($route, $options)
    {
        $this->current = new Route($route, $options);
    }

    protected function allParamsPresent($given, $required)
    {
        $matchingQueryParams = array_intersect($given, $required);

        return count($matchingQueryParams) == count($required);
    }
    
    /**
     * @return Route
     */
    public function current()
    {
        return $this->current;
    }

    public function make($name, array $params = [])
    {
        list($url, $options) = $this->routes->named($name);

        if(array_key_exists('inQuery', $options)){
            if(!$this->allParamsPresent(array_keys($params), $options['inQuery'])){
                throw new RequiredParametersMissingException('Required route parameters missing:' . implode(',', array_diff($options['inQuery'], array_keys($params))));
            }
        }

        $qs = http_build_query($params);

        return $this->request->baseUrl() . $url . ($qs ? '?' . $qs : '');
    }
}