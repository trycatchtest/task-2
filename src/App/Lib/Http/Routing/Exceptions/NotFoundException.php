<?php namespace App\Lib\Http\Routing\Exceptions;

use App\Lib\Core\Exception;

class NotFoundException extends Exception
{
    protected $status = 404;
}