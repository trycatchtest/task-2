<?php namespace App\Lib\Http\Routing\Exceptions;

use App\Lib\Core\Exception;

class RequiredParametersMissingException extends Exception
{

}