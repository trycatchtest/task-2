<?php namespace App\Lib\Http\Routing;

use App\AddressBook\Address\Exceptions\NotFoundException;

class RouteCollection
{
    /**
     * @var array
     */
    protected $items;
    /**
     * @var array
     */
    protected $named;

    public function __construct()
    {
        $this->items = [];
        $this->named = [];
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function get($url, array $options = [])
    {
        $this->register($url, $options, 'GET');
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function post($url, array $options = [])
    {
        $this->register($url, $options, 'POST');
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function put($url, array $options = [])
    {
        $this->register($url, $options, 'PUT');
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function patch($url, array $options = [])
    {
        $this->register($url, $options, 'PATCH');
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function delete($url, array $options = [])
    {
        $this->register($url, $options, 'DELETE');
    }

    /**
     * @param $url
     * @param $handlerClass
     * @param $namePrefix
     * @param string $keyQueryParam
     */
    public function rest($url, $handlerClass, $namePrefix, $keyQueryParam = 'id')
    {
        $this->get($url, ['action' => $handlerClass.'::read', 'inQuery' => [$keyQueryParam], 'name' => $namePrefix . 'read']);
        $this->put($url, ['action' => $handlerClass.'::update', 'inQuery' => [$keyQueryParam], 'name' => $namePrefix . 'update']);
        $this->delete($url, ['action' => $handlerClass.'::delete', 'inQuery' => [$keyQueryParam], 'name' => $namePrefix . 'delete']);
        $this->post($url, ['action' => $handlerClass.'::create', 'name' => $namePrefix . 'create']);
        $this->get($url, ['action' => $handlerClass.'::index', 'name' => $namePrefix . 'index']);
    }


    /**
     * @param string $url
     * @param array $options
     * @param string $requestMethod
     */
    public function register($url, array $options = [], $requestMethod = 'GET')
    {
        $options['method'] = $requestMethod;

        if(!array_key_exists($url, $this->items)){
            $this->items[$url] = array();
        }

        $newIdx = count($this->items[$url]);

        $this->items[$url][$newIdx] = $options;

        if(array_key_exists('name', $options)){
            $this->named[$options['name']] = [$url => $newIdx];
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function named($name)
    {
        if(!array_key_exists($name, $this->named)){
            throw new NotFoundException($name);
        }

        list($url, $optionsIdx) = each($this->named[$name]);

        return [$url, $this->items[$url][$optionsIdx]];
    }
}