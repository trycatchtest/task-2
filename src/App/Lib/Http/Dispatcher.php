<?php namespace App\Lib\Http;

use App\Lib\Http\Exceptions\ControllerMethodNotFoundException;
use App\Lib\Http\Exceptions\ControllerNotFoundException;
use App\Lib\Http\Response\Response;
use App\Lib\Http\Routing\Route;

/**
 * Class Dispatcher
 * @package  App\Lib\Http
 */
class Dispatcher
{
    /**
     * @var Route
     */
    protected $route;

    protected $app;

    protected $controller;

    protected $controllerClass;

    protected $controllerMethod;

    protected $content;

    protected $status;

    public function __construct(HttpClient $app)
    {
        $this->app = $app;
    }

    /**
     * @param Route $route
     * @return Response
     * @throws ControllerNotFoundException
     */
    public function dispatch(Route $route)
    {
        $this->status = 200;

        $this->route = $route;

        $this->parseAction();

        $this->makeController();

        $this->runAction();

        return $this->makeResponse();
    }

    protected function parseAction()
    {
        list($this->controllerClass, $this->controllerMethod) = explode('::', $this->route->action());
    }

    protected function makeController()
    {
        if(!class_exists($this->controllerClass)){
            throw new ControllerNotFoundException($this->controllerClass);
        }

        $this->controller = $this->app->make($this->controllerClass, [$this->app]);

        $this->injectDependencies();
    }

    protected function injectDependencies()
    {
        if(method_exists($this->controller, 'setApp')){
            $this->controller->setApp($this->app);
        }

        if(method_exists($this->controller, 'setQuery')){
            $this->controller->setQuery($this->app->request()->query());
        }

        if(method_exists($this->controller, 'setData')){
            $this->controller->setData($this->app->request()->data());
        }
    }

    protected function runAction()
    {
        if(!is_callable([$this->controller, $this->controllerMethod])){
            throw new ControllerMethodNotFoundException(sprintf('%s::%s', $this->controllerClass, $this->controllerMethod));
        }

        $this->content = call_user_func_array([$this->controller, $this->controllerMethod], []);
    }

    /**
     * @return Response
     */
    protected function makeResponse()
    {
        if($this->content instanceof Response){
            return $this->content;
        }

        return new Response($this->content, $this->status);
    }
}