<?php namespace App\Lib\Http;

use App\Lib\Common\Collection;

class Request
{
    /**
     * @var Collection
     */
    protected $requested;

    /**
     * @var Collection
     */
    protected $query;

    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var string
     */
    protected $method;

    /**
     * @param array $server
     */
    public function __construct(array $server = [])
    {
        $this->requested = $requested = new Collection($server ?: $_SERVER);

        parse_str($requested->get('QUERY_STRING'), $query);

        $this->query = new Collection($query);

        $this->method = $requested->get('REQUEST_METHOD', 'GET');

        $this->setData();
    }

    protected function setData()
    {
        if($this->isPost()){
            $this->data = new Collection($_POST);
        }
        else{
            $this->data = new Collection($this->findRequestData());
        }
    }

    /**
     * @return array
     */
    protected function findRequestData()
    {
        $vars = trim(file_get_contents('php://input'));

        $data = [];

        parse_str($vars, $data);

        return $data;
    }

    /**
     * @return string
     */
    public function path()
    {
        return $this->requested->get('PATH_INFO');
    }

    /**
     * @param string $key
     * @return string
     */
    public function query($key = null)
    {
        if(is_null($key)){
            return $this->query;
        }

        return $this->query->get($key);
    }

    /**
     * @param string $key
     * @return string
     */
    public function data($key = null)
    {
        if(is_null($key)){
            return $this->data;
        }

        return $this->data->get($key);
    }

    /**
     * @return bool
     */
    public function isSecure()
    {
        return ($this->requested->get('REQUEST_SCHEME') == 'https');
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return ($this->method == 'GET');
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return ($this->method == 'POST');
    }

    /**
     * @return bool
     */
    public function isPut()
    {
        return ($this->method == 'PUT');
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return ($this->method == 'DELETE');
    }

    /**
     * @return bool
     */
    public function isPatch()
    {
        return ($this->method == 'PATCH');
    }

    /**
     * @return string
     */
    public function method()
    {
        return $this->method;
    }

    public function baseUrl()
    {
        $scheme = $this->requested->get('REQUEST_SCHEME', 'http');
        $host = $this->requested->get('HTTP_HOST');
        $port = $this->requested->get('SERVER_PORT', 80);
        $script = $this->requested->get('SCRIPT_NAME');

        $url = $scheme . '://' . $host;

        if(!(($scheme == 'http' && $port == 80) || ($scheme == 'https' && $port == 443))){
            $url .= ':' . $port;
        }

        $url .= $script;

        return $url;
    }

}