<?php namespace App\Lib\Http;

use App\Lib\Common\Collection;
use App\Lib\Common\InjectableInterface;

class Controller implements InjectableInterface
{
    use ControllerTrait;

    /**
     * @var HttpClient
     */
    protected $app;

    /**
     * @var Collection
     */
    protected $query;

    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var array
     */
    protected $injectable = [];

    public function __construct(HttpClient $app)
    {
        $this->app = $app;
    }

    public function getInjectable()
    {
        return $this->injectable;
    }
}