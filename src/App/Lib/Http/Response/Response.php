<?php namespace App\Lib\Http\Response;

use App\Lib\Common\ArrayableInterface;
use App\Lib\Common\Collection;

class Response
{
    protected $content;

    protected $status;

    protected $headers;

    public function __construct($content = '', $status = 200, array $headers = [])
    {
        $this->content = $content;

        $this->status = $status;

        $this->headers = new Collection($headers);
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function headers()
    {
        return $this->headers;
    }

    public function send()
    {
        $this->parseContent();

        $this->sendHeaders();

        $this->sendContent();
    }

    protected function parseContent()
    {
        if(is_string($this->content)){
            $this->content = $this->htmlResponse($this->content);
            return;
        }

        if(is_array($this->content)){
            $this->content = $this->jsonResponse($this->content);
            return;
        }

        if(is_object($this->content)){
            if($this->content instanceof ArrayableInterface){
                $this->content = $this->jsonResponse($this->content->toArray());
                return;
            }
        }

        $this->errorResponse('UnknownContentType');
    }

    protected function htmlResponse($data)
    {
        $this->headers->put('Content-Type', 'text/html');

        return $data;
    }

    protected function jsonResponse($data)
    {
        $this->headers->put('Content-Type', 'application/json');

        return json_encode($data);
    }

    protected function errorResponse($error)
    {
        $this->status = 500;
        $this->content = $this->jsonResponse(['error' => $error]);

    }

    protected function sendHeaders()
    {
        if(headers_sent()){
            return;
        }

        http_response_code($this->status);

        foreach($this->headers->getIterator() as $header => $content){
            header(sprintf('%s: %s', $header, $content));
        }
    }

    protected function sendContent()
    {
        echo $this->content;
    }
}