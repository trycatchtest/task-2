<?php namespace App\Lib\Http\Response;

use App\Lib\Core\Exception;

class ErrorResponse extends Response
{
    protected $debug;

    public function __construct(Exception $e, $debug = 0)
    {
        $this->debug = $debug;

        parent::__construct($this->errorContent($e), $e->getStatus());
    }

    protected function errorContent(Exception $e)
    {
        if($this->debug){
            return ['error' => get_class($e), 'message' => $e->getMessage()];
        }
        else{
            return ['error' => get_class($e)];
        }
    }

}