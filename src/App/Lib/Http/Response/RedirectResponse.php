<?php namespace App\Lib\Http\Response;

class RedirectResponse extends Response
{
    public function __construct($location, $status = 302, $content = '')
    {
        parent::__construct($content, $status);

        $this->headers->put('Location', $location);
    }

}