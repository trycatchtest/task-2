<?php namespace App\Lib\Http;

use App\Lib\Core\Application;
use App\Lib\Core\Exception;
use App\Lib\Http\Response\ErrorResponse;
use App\Lib\Http\Routing\Router;

class HttpClient extends Application
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @param array $paths
     * @param Request $request
     * @param Router $router
     * @param Dispatcher $dispatcher
     */
    public function __construct(array $paths = [], Request $request = null, Router $router = null, Dispatcher $dispatcher = null)
    {
        $this->request = $request ?: new Request();

        $this->router = $router ?: new Router($this->request);

        $this->dispatcher = $dispatcher ?: new Dispatcher($this);

        parent::__construct($paths);
    }

    /**
     * @throws Routing\NotFoundException
     */
    public function run()
    {
        try {
            $this->router->match();

            $response = $this->dispatcher->dispatch($this->router()->current());
        }
        catch(Exception $e){
            $response = new ErrorResponse($e, $this->config->get('app.debug'));
        }

        $response->send();
    }

    /**
     * @return Router
     */
    public function router()
    {
        return $this->router;
    }

    /**
     * @return Request
     */
    public function request()
    {
        return $this->request;
    }

}