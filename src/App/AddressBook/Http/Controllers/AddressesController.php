<?php namespace App\AddressBook\Http\Controllers;

use App\AddressBook\Address\AddressService;
use App\AddressBook\Address\Repositories\RepositoryInterface;
use App\Lib\Datasources\Exceptions\ResourceNotFoundException;
use App\Lib\Http\Controller;
use App\Lib\Http\Response\RedirectResponse;
use App\Lib\Http\Response\Response;

class AddressesController extends Controller
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;
    /**
     * @var AddressService
     */
    protected $service;

    protected $injectable = [
        'service' => 'App\AddressBook\Address\AddressService'
    ];

    public function setService(AddressService $service)
    {
        $this->service = $service;
    }


    public function index()
    {
        return $this->service->getRepository()->all();
    }

    public function read()
    {
        $key = $this->query->get('id');

        $address = $this->service->getRepository()->one($key);

        return $address;
    }

    public function create()
    {
        $address = $this->service->create($this->data->all());

        $resourceLocation = $this->app->router()->make('address.read', ['id' => $address->getKey()]);

        return new RedirectResponse($resourceLocation, 201, $address);
    }

    public function update()
    {
        $key = $this->query->get('id');

        try{
            $address = $this->service->getRepository()->one($key);

            $this->service->update($address, $this->data->all());
        }
        catch (ResourceNotFoundException $e){
            return $this->create();
        }

        return new Response('', 204);
    }

    public function delete()
    {
        $key = $this->query->get('id');

        $this->service->delete($key);

        return new Response('', 204);
    }
}