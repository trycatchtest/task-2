<?php namespace App\AddressBook;

use App\Lib\Common\Collection;
use App\Lib\Datasources\Csv;
use App\Lib\Http\HttpClient;
use App\Lib\Core\BaseServiceProvider;
use App\Lib\Http\Routing\RouteCollection;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * @var Collection
     */
    protected $config;

    public function bootstrap()
    {
        $this->setup();

        $this->makeServices();

        $this->makeCsvRepositories();

        if($this->httpClient()){
            $this->setupHttp($this->app);
        }
    }

    protected function setup()
    {
        $this->app->config()->load('address_book');

        $this->config = $this->app->config()->get('address_book');
    }

    protected function makeCsvRepositories()
    {
        $resourceDir = $this->app->paths()->get($this->config->get('resourcePath')) . DIRECTORY_SEPARATOR;

        $repositoryConfig = $this->config->get('repositoryConfig');

        foreach($this->config->get('csv-repositories', []) as $abstract => $concrete){
            $resourcePath = $resourceDir . $repositoryConfig[$concrete]['table'];

            $dataSource = new Csv($resourcePath, $repositoryConfig[$concrete]['map']);

            $this->app->bind($abstract, new $concrete($dataSource));
        }
    }

    protected function makeServices()
    {
        foreach($this->config->get('services', []) as $service){
            $this->app->bind($service, new $service);
        }

    }

    protected function setupHttp(HttpClient $app)
    {
        $this->registerRoutes($app->router()->routes());
    }

    protected function registerRoutes(RouteCollection $routes)
    {
        $routes->rest('/address', 'App\AddressBook\Http\Controllers\AddressesController', 'address.');
    }
}