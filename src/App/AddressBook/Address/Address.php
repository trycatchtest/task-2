<?php namespace App\AddressBook\Address;

use App\Lib\Common\Entity;

class Address extends Entity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $street;

    /**
     * @var array
     */
    protected $fillable = ['name', 'phone', 'street'];

    /**
     * @param mixed $key
     * @param array $data
     * @param bool $exists
     * @param bool $dirty
     */
    public function __construct(array $data = [], $key = null, $exists = true, $dirty = false)
    {
        parent::__construct($key, $exists, $dirty);

        $this->setAttributes($data);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }


}