<?php namespace App\AddressBook\Address;

use App\AddressBook\Address\Repositories\RepositoryInterface;
use App\Lib\Common\Service;
use App\Lib\Datasources\Exceptions\ResourceNotFoundException;

class AddressService extends Service
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;
    /**
     * @var AddressValidator
     */
    protected $validator;

    protected $injectable = [
        'repository' => 'App\AddressBook\Address\Repositories\RepositoryInterface',
        'validator' => 'App\AddressBook\Address\AddressValidator',
    ];

    /**
     * @param array $data
     * @return Address
     */
    public function create(array $data = [])
    {
        $this->validates('create', $data);

        $address = $this->repository->makeModel($data, null, false, true);

        $this->repository->persist($address);

        return $address;
    }

    /**
     * @param Address $address
     * @param $data
     */
    public function update(Address $address, $data)
    {
        $this->validates('update', $data);

        $address->setAttributes($data);

        $this->repository->persist($address);
    }

    /**
     * @param $key
     */
    public function delete($key)
    {
        try{
            $this->repository->delete($key);
        }
        catch(ResourceNotFoundException $e){}
    }

    public function validates($type = 'create', array $data = [])
    {
        $methodName = 'validates' . ucfirst($type);

        $this->validator->$methodName($data);
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param AddressValidator $validator
     */
    public function setValidator(AddressValidator $validator)
    {
        $this->validator = $validator;
    }

}