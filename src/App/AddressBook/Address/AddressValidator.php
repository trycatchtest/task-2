<?php namespace App\AddressBook\Address;

use App\Lib\Validation\Validator;

class AddressValidator extends Validator
{
    public function validatesCreate($data)
    {
        $rules = [
            'name' => ['required'],
            'phone' => ['required', 'numeric', 'length:9'],
            'street' => ['required'],
        ];

        $this->validate($data, $rules);
    }

    public function validatesUpdate($data)
    {
        $rules = [
            'name' => ['if-data-exists', 'required'],
            'phone' => ['if-data-exists', 'required', 'numeric', 'length:9'],
            'street' => ['if-data-exists', 'required'],
        ];

        $this->validate($data, $rules);
    }

}