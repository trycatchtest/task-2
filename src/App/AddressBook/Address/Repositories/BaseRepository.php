<?php namespace App\AddressBook\Address\Repositories;

use App\AddressBook\Address\Address;
use App\Lib\Common\EntityCollection;

abstract class BaseRepository implements RepositoryInterface
{

    /**
     * @param array $data
     * @param mixed $key
     * @param bool $exists
     * @param bool $dirty
     * @return Address
     */
    public function makeModel(array $data = [], $key = null, $exists = true, $dirty = false)
    {
        return new Address($data, $key, $exists, $dirty);
    }

    /**
     * @param array $models
     * @return EntityCollection
     */
    public function makeCollection(array $models = [])
    {
        return new EntityCollection($models);
    }
}