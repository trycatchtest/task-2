<?php namespace App\AddressBook\Address\Repositories;

use App\AddressBook\Address\Address;
use App\Lib\Datasources\Csv;

class CsvRepository extends BaseRepository
{
    /**
     * @var Csv
     */
    protected $datasource;

    /**
     * @param Csv $datasource
     */
    public function __construct(Csv $datasource)
    {
        $this->datasource = $datasource;
    }

    /**
     * @return \App\Lib\Common\EntityCollection
     */
    public function all()
    {
        $rows = $this->datasource->read();
        $models = [];

        foreach($rows as $key => $data){
            $models[] = $this->makeModel($data, $key);
        }

        return $this->makeCollection($models);
    }

    /**
     * @param $key
     * @return Address
     */
    public function one($key)
    {
        $data = $this->datasource->read($key);

        return $this->makeModel($data, $key);
    }

    /**
     * @param Address $address
     */
    public function persist(Address $address)
    {
        if(!$address->dirty()){
            return;
        }

        $data = $address->getAttributes();

        if($address->exists()){
            $this->datasource->update($address->getKey(), $data);

            $address->setDirty(false);
        }
        else{
            $key = $this->datasource->create($data);

            $address->stored($key);
        }
    }

    /**
     * @param $key
     */
    public function delete($key)
    {
        $this->datasource->delete($key);
    }

}