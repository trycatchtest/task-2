<?php namespace App\AddressBook\Address\Repositories;

use App\AddressBook\Address\Address;
use App\Lib\Common\EntityCollection;

interface RepositoryInterface
{
    /**
     * @return EntityCollection
     */
    public function all();

    /**
     * @param $key
     * @return Address
     */
    public function one($key);


    /**
     * @param Address $address
     */
    public function persist(Address $address);

    /**
     * @param $key
     */
    public function delete($key);

    /**
     * @param array $data
     * @param mixed $key
     * @param bool $exists
     * @param bool $dirty
     * @return Address
     */
    public function makeModel(array $data = [], $key = null, $exists = true, $dirty = false);

    /**
     * @param array $models
     * @return EntityCollection
     */
    public function makeCollection(array $models = []);
}