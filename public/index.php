<?php
$baseDir = dirname(dirname(__FILE__));

$paths = require_once $baseDir . DIRECTORY_SEPARATOR . 'bootstrap' . DIRECTORY_SEPARATOR . 'paths.php';

require_once $paths['bootstrap'] . DIRECTORY_SEPARATOR . 'autoload.php';
require_once $paths['bootstrap'] . DIRECTORY_SEPARATOR . 'helpers.php';

$httpClient = new \App\Lib\Http\HttpClient($paths);

$httpClient->run();
